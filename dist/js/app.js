// Bank some good vibe options
// const vibes = [
//     "...and you are awesome!",
//     "...have a wonderful day!",
//     "...and you've got this!",
//     "...and so is this puppy! 🐶"
// ];

// // choose a random good vibe
// var vibe = vibes[Math.floor(Math.random() * Math.floor(vibes.length))];

// // display a good vibe
// document.querySelector(".vibe").append(vibe);

function geoFindMe() {

    const status = document.querySelector('#status');
    const mapLink = document.querySelector('#map-link');

    mapLink.href = '';
    mapLink.textContent = '';

    function success(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        status.textContent = '';
        mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude.toFixed(4)}/${longitude.toFixed(4)}`;
        mapLink.textContent = `Latitude: ${latitude.toFixed(4)} °, Longitude: ${longitude.toFixed(4)} °`;
    }

    function error() {
        status.textContent = 'Unable to retrieve your location';
    }

    if (!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }

}

document.querySelector('#find-me').addEventListener('click', geoFindMe);