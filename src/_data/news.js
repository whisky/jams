const axios = require("axios");
require("dotenv").config();

module.exports = async function () {
    news_url = `https://newsapi.org/v2/top-headlines?country=tw&apiKey=${process.env.API_KEY}`
    try {
        const res = await axios.get(news_url);
        console.log(res.data)
        return res.data;
    } catch (err) {
        await fetch(news_url)
        console.error(err);
    }
};