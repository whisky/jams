module.exports = function (config) {

    config.setBrowserSyncConfig({
        host: 'dark.inc',
        https: {
            key: 'd:\\SSL\\dark.inc.key',
            cert: 'd:\\SSL\\dark.inc.crt'
        }
    });
    config.addPassthroughCopy("src/js");
    return {
        dir: {
            input: "src",
            output: "dist",
            data: "_data"
        }
    };

};
